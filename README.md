# Crypto Market Microstructure Analysis

This README provides an overview of the project, discusses implications, and provides setup guidelines and potential further avenues to explore.

## Team

Ashrith Anumala

Hi! I'm a Computer Science and Statistics Major planning to graduate in May 2026! I'm interested in data science, quantitative finance, and machine learning roles. I'm especially interested in the application of big data to new fields that haven't been explored before.

![Alt text](img/color_headshot.JPG){width=140 height=140px}

[Email](mailto:anumala3@illinois.edu)

[LinkedIn](https://www.linkedin.com/in/ashrithanumala/)

## Project Summary

**Objective**
My main objective with this project is to uncover differences in the crypto market due to the arrival of new Crypto ETFs beginning in January 2024. This project serves as a starting point for analyzing very recent trends and relationships between various exchanges around the world that trade BTC & ETH.

## Approach
To handle this huge amount of data locally, I leveraged a newer python library known as dask. What is extremely useful about Dask is that it leverages multiprocessing to load in data "lazily" which means its able to accumulate all the data together in one location, and then can quickly merge all of it together. This was super benificial when dealing with various files in this case. Another useful library was Lead-Lag. This is a library made by Phillipe Remy, who I reached out to personally for advice for both usage and implementation within my project. This library was able to taken in 2 time series and analyze the correlation between different combinations of lags applied to either of the two. This allowed me to see which exchange was in the "Lead" position in changing the price globally.

## Future Steps
If anyone enjoyed the development of this project, these are a couple future directions I plan to take with it. First off, I want to conduct analysis on the relationship between market book data and it's effect on prices across exchanges using some form of a Neural Network. I also hope to find more significant correlations between price, volume, and volatility through more statistical based modeling methods.
